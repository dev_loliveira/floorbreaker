package com.redcode.floorbreaker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class AbstractScreen implements Screen, DataManagement {
	public  static int           BUTTON_WIDTH = 250;
	public  static int           BUTTON_HEIGHT = 60;
	private FloorBreaker             _game;
	private Skin                  _skin;
	private Stage                 _stage;
	
	public AbstractScreen (FloorBreaker game) {
		_game = game;
	}
	
	public void update (double delta) {
		
	}
	
	public void update () {
		
	}
	
	public void render() {
		
	}
	
	public void render (float delta) {
		
	}
	
	public Stage getStage () {
		if (_stage == null) {
			_stage = new Stage(FloorBreaker.VIRTUAL_WIDTH, FloorBreaker.VIRTUAL_HEIGHT, true);
		}
		
		Gdx.input.setInputProcessor(_stage);
		return (_stage);
	}
	
	public Skin getSkin() {
		if (_skin == null) {
			_skin = new Skin(Gdx.files.internal("data/uiskin.json"));
		}
		
		return (_skin);
	}
	
	public void unload() {
		_stage.clear();
	}
}
