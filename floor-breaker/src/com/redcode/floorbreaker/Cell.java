package com.redcode.floorbreaker;

public class Cell {
	public static final char          CELL_EMPTY = '0';
	public static final char          CELL_FILLED = '1';
	public static final int           CELL_SIZE = 75;
	public static final int           CELL_GAP = 1;
	public static final int           BORDER_GAP = 100;
	
	private int                         _row;
	private int                         _col;
	private int                         _x;
	private int                         _y;
	private int                         _resistance;
	private boolean                    _isGround;
	
	private int                        _origRow;
	private int                        _origCol;
	private int                        _origX;
	private int                        _origY;
	private int                        _origResistance;
	
	public Cell(int row, int col, int x, int y, int resistance) {
		_row = row;
		_col = col;
		_x = x;
		_y = y;
		_resistance = resistance;
		
		_origRow = row;
		_origCol = col;
		_origX = x;
		_origY = y;
		_origResistance = resistance;
		
		_isGround = resistance > 0 ? true : false;
	}
	
	public boolean wasGround() {
		return (_isGround);
	}
	
	public static int getInitialX() {
		int x = BORDER_GAP;
		return (x);
	}
	
	public static int getInitialY() {
		int y = BORDER_GAP+CELL_SIZE;
		return (y);
	}
	
	public void setRow(int row) {
		_row = row;
	}
	
	public int getRow () {
		return (_row);
	}
	
	public void setCol(int col) {
		_col = col;
	}
	
	public int getCol() {
		return (_col);
	}
	
	public int getX() {
		return (_x);
	}
	
	public int getY() {
		return (_y);
	}
	
	public int getResistance() {
		return (_resistance);
	}
	
	public int getOriginalRow() {
		return (_origRow);
	}

	public int getOriginalCol() {
		return (_origCol);
	}

	public int getOriginalX() {
		return (_origX);
	}

	public int getOriginalY() {
		return (_origY);
	}
	
	public void restart () {
		_row = _origRow;
		_col = _origCol;
		_x = _origX;
		_y = _origY;
		_resistance = _origResistance;
	}

	public int getOriginalResistance() {
		return (_origResistance);
	}
	
	public void decreaseResistance () {
		_resistance = _resistance > 0 ? _resistance-1 : 0;
	}
}
