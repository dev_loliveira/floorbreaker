package com.redcode.floorbreaker;

import com.badlogic.gdx.utils.Array;

public class GameGrid {
	private FloorBreaker              _game;
	private Array<Cell>            _cells;
	
	public GameGrid (FloorBreaker game) {
		_game = game;
		_cells = new Array<Cell>();
	}
	
	public void addCell(int row, int col, int x, int y, int resistance) {
		_cells.add(new Cell(row, col, x, y, resistance));
	}
	
	public Array<Cell> getCells() {
		return (_cells);
	}
	
	public Cell getCell(int row, int col) {
		for(Cell cell: _cells) {
			if (cell.getRow() == row && cell.getCol() == col) {
				return (cell);
			}
		}
		
		return (null);
	}
	
}
