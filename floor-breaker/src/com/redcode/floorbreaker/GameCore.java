package com.redcode.floorbreaker;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import sun.misc.IOUtils;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Audio;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;


public class GameCore extends AbstractScreen implements InputProcessor {
	private enum STATE {
		Loading,
		Running,
		SolvedAllPuzzles
	}
	
	
	private final int                    MAX_WAITING_TIME = 1;
	private STATE                         _state;
	private FloorBreaker                  _game;
	private int                           _level;
	private GameGrid                      _gameGrid;
	private TextureRegion                 _imgPlayer;
	private TextureRegion                 _imgCellResistance1;
	private TextureRegion                 _imgCellResistance2;
	private TextureRegion                 _imgCellEmpty;
	private TextureRegion                 _imgCellDestroyed;
	private TextureRegion                 _background;
	private BitmapFont                    _fontLoading;
	private BitmapFont                    _fontInformation;
	private LanguagesManager              _lang;
	private float                        _totalTime;
	private NinePatch                     _menuDefault;
	private int                           _tries;
	private Sound                         _sndSwitch;
	private Music                         _music;
	private Cell                          _playerCell;
	private Cell                          _originalPlayerCell;
	private int                           _movementsCount;
	private float                         _lastScenarioWaiting;

	
	
	public GameCore(FloorBreaker game, int level) {
		super(game);
		_game = game;
		_state = STATE.Loading;
		_lang = LanguagesManager.getInstance();
		_level = level;
		
		configureLevel();
		configurePlayer();
		
		if (Gdx.app.getType() == ApplicationType.Android) {
		}
	}
	
	public void reset () {
		_totalTime = 0;
		_movementsCount = 0;
		_lastScenarioWaiting = 0;
	}

	@Override
	public void load() {
		AssetManager assetManager = _game.getAssetManager();
		_fontLoading = _game.getPlatformResolver().loadFont("data/default.fnt", "data/mksanstallx.ttf", 30);
		assetManager.load("data/player.png", Texture.class);
		assetManager.load("data/background.png", Texture.class);
		assetManager.load("data/menuDefault.png", Texture.class);
		assetManager.load("data/cellresistance1.png", Texture.class);
		assetManager.load("data/cellresistance2.png", Texture.class);
		assetManager.load("data/cellempty.png", Texture.class);
		assetManager.load("data/celldestroyed.png", Texture.class);
		assetManager.load("data/light_switch.wav", Sound.class);
		//assetManager.load("data/music.mp3", Music.class);
	}

	@Override
	public void unload() {
		AssetManager assetManager = _game.getAssetManager();
		_fontLoading = null;
		_totalTime = 0;
		_menuDefault = null;
		_sndSwitch = null;
		_imgCellResistance1 = null;
		_imgCellResistance2 = null;
		_imgCellEmpty = null;
		_imgCellDestroyed = null;
		assetManager.unload("data/player.png");
		assetManager.unload("data/background.png");
		assetManager.unload("data/menuDefault.png");
		assetManager.unload("data/cellresistance1.png");
		assetManager.unload("data/cellresistance2.png");
		assetManager.unload("data/cellempty.png");
		assetManager.unload("data/celldestroyed.png");
		if (_game.isSoundEnabled()) {
		    assetManager.unload("data/light_switch.wav");
		}

		if (_game.getRequestHandler() != null && Gdx.app.getType() == ApplicationType.Android)
			_game.getRequestHandler().showAds(false);
	}

	@Override
	public void assignResources() {
		AssetManager assetManager = _game.getAssetManager();
		_movementsCount = 0;
		_lastScenarioWaiting = 0;
		_tries = 0;
		_fontInformation = _game.getPlatformResolver().loadFont("data/default.fnt", "data/mksanstallx.ttf", 30);
		
		_imgPlayer = new TextureRegion(assetManager.get("data/player.png", Texture.class));
		
		_imgCellEmpty = new TextureRegion(assetManager.get("data/cellempty.png", Texture.class));
		_imgCellDestroyed = new TextureRegion(assetManager.get("data/celldestroyed.png", Texture.class));
		_imgCellResistance1 = new TextureRegion(assetManager.get("data/cellresistance1.png", Texture.class));
		_imgCellResistance2 = new TextureRegion(assetManager.get("data/cellresistance2.png", Texture.class));
		
		_background = new TextureRegion(assetManager.get("data/background.png", Texture.class));

		_menuDefault = new NinePatch(assetManager.get("data/menuDefault.png", Texture.class), 8, 8, 8, 8);
		
		if (_game.isSoundEnabled()) {
			_sndSwitch = assetManager.get("data/light_switch.wav", Sound.class);
			//_music = assetManager.get("data/music.mp3", Music.class);
		}

		Gdx.input.setInputProcessor(this);
		
        // show the ads after loading is complete
		if (_game.getRequestHandler() != null && Gdx.app.getType() == ApplicationType.Android)
			_game.getRequestHandler().showAds(true);
	}

	public void update(double deltaT) {
		AssetManager assetManager = _game.getAssetManager();
		if (_state == STATE.Loading) {
			if (assetManager.update()) {
					assignResources();
					_state = STATE.Running;
			}
			
			else {
				_state = STATE.Loading;
			}
		}
	}

	@Override
	public void render(float delta) {
		SpriteBatch batch = _game.getSpriteBatch();
		if (_state == STATE.Loading) {
			String loading = _lang.getString("Loading...") + Integer.toString((int)(_game.getAssetManager().getProgress()*100))+" %";
			TextBounds bounds = _fontLoading.getBounds(loading);
			_fontLoading.draw(batch, loading, (FloorBreaker.VIRTUAL_WIDTH-bounds.width)/2, (FloorBreaker.VIRTUAL_HEIGHT-bounds.height)/2);
			update(delta);
		}
		
		else if(_state == STATE.Running) {
			batch.draw(_background, 0, 0);
			
			for (Cell cell: _gameGrid.getCells()) {
				Vector3 cellPos = new Vector3();
				cellPos.set(cell.getX(), cell.getY(), 0);
				_game.getCamera().unproject(cellPos);

			
				switch (cell.getResistance()) {
					case 0:
						if (cell.wasGround()) { //celula destruida
						    batch.draw(_imgCellDestroyed, cellPos.x, cellPos.y);
						}
						
						else { //nunca foi chao, portanto e celula vazia
						    batch.draw(_imgCellEmpty, cellPos.x, cellPos.y);
						}
						break;
					
					case 1:
						batch.draw(_imgCellResistance1, cellPos.x, cellPos.y);
						break;

					case 2:
						batch.draw(_imgCellResistance2, cellPos.x, cellPos.y);
						break;
				}
				
				if (_playerCell == cell) {
					batch.draw(
							_imgPlayer,
							cellPos.x + _imgPlayer.getRegionWidth()/2,
							cellPos.y + _imgPlayer.getRegionHeight()/2
							);
				}
			}
			
			renderAdditionalInformation();
			_totalTime += delta;
			
			if (hasWon()) {
				
				Preferences p = Gdx.app.getPreferences("preferences");
				if (p.getInteger("lastUnlockedLevel") < _level+1) {
					p.putInteger("lastUnlockedLevel", _level+1);
					p.flush();
				}
				
				if (_game.stageExists(_level+1)) {
					_level++;
					reset();
					configureLevel();
					configurePlayer();
				}
				
				else {
					_state = STATE.SolvedAllPuzzles;
				}
			}
			
			if (_playerCell.getResistance() <= 0) { //Restart
				restart();
			}
		}
		
		else if (_state == STATE.SolvedAllPuzzles) {
			String congratulations = _lang.getString("Congratulations, you won all challenges!");
			TextBounds congratulationsBounds = _fontInformation.getBounds(congratulations);
			_fontInformation.draw(
					batch, 
					congratulations, 
					(FloorBreaker.VIRTUAL_WIDTH-congratulationsBounds.width)/2 , 
					(FloorBreaker.VIRTUAL_HEIGHT+congratulationsBounds.height)/2);

			if (_lastScenarioWaiting < MAX_WAITING_TIME) {
				_lastScenarioWaiting += delta;
			}
		}
	}
	
	public boolean hasWon() {
		if (getRemainingCells() == 1 && _playerCell.getResistance() == 1) return (true);
		return (false);
	}
	
	public int getRemainingCells() {
		int count = 0;
		for (Cell cell: _gameGrid.getCells()) {
			if (cell.getResistance() > 0) count++;
		}
		
		return (count);
	}
	
	public void renderAdditionalInformation() {
		SpriteBatch batch = _game.getSpriteBatch();
		Rectangle menuRect = new Rectangle();
		menuRect.x = 0;
		menuRect.y = 0;
		menuRect.width = FloorBreaker.VIRTUAL_WIDTH;
		menuRect.height = 80;
		
		_menuDefault.draw(
				batch,
				menuRect.x, menuRect.y,
				menuRect.width, menuRect.height
				);
		
		String elapsedTime = _lang.getString("Time elapsed");
		String levelName = _lang.getString("Level");
		String remainingCellsStr = _lang.getString("Remaining floors");
		
		TextBounds elapsedTimeBounds = _fontInformation.getBounds(elapsedTime);
		TextBounds levelBounds = _fontInformation.getBounds(levelName);
		TextBounds remainingCellsBounds = _fontInformation.getBounds(remainingCellsStr);
		
		int elapsedTimeTotalWidth = (int)(elapsedTimeBounds.width + _fontInformation.getBounds(String.format(": %.0f", _totalTime)).width);
		int levelTotalWidth = (int)(levelBounds.width + _fontInformation.getBounds(String.format(": %d", _level)).width);
		int remainingCellsTotalWidth = (int)(remainingCellsBounds.width + _fontInformation.getBounds(String.format(": %d", getRemainingCells())).width);
		
		_fontInformation.draw(
				batch,
				String.format("%s: %.0f", elapsedTime, _totalTime),
				menuRect.x+10,
				menuRect.y+menuRect.height-elapsedTimeBounds.height
				);

		_fontInformation.draw(
				batch,
				String.format("%s: %d", levelName, _level),
				menuRect.width/2 - levelBounds.width/2,
				menuRect.y+menuRect.height-elapsedTimeBounds.height
				);

		_fontInformation.draw(
				batch,
				String.format("%s: %d", remainingCellsStr, getRemainingCells()),
				menuRect.width - remainingCellsTotalWidth*4,
				menuRect.y+menuRect.height-elapsedTimeBounds.height
				);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean keyDown(int keycode) {
		if (keycode == Keys.BACK || keycode == Keys.ESCAPE) {
			_game.setCurrentScreen( new MenuStartGame(_game) );
		}
		
		else if (keycode == Keys.LEFT) {
			
		}
		
		else if (keycode == Keys.RIGHT) {
			
		}
		
		else if (keycode == Keys.UP) {
			
		}
		
		else if (keycode == Keys.DOWN) {
			
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if (_state == STATE.Running) {
			Vector3 tpos = new Vector3();
			tpos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			_game.getCamera().unproject(tpos);
			
			Cell nextCell = getCellByCoord(Gdx.input.getX(), Gdx.input.getY());
			if (nextCell != null) {
				if (playerCanGo(nextCell) && nextCell.wasGround()) {
					playerMove(nextCell);
					if (_game.isSoundEnabled()) _game.getFloorDestructionSound().play(1);
				}
				
				else {
				}
				
			}
			
			else {
			}
			
		}
		
		else if (_state == STATE.SolvedAllPuzzles) {
			if (_lastScenarioWaiting >= MAX_WAITING_TIME)
			    _game.setCurrentScreen( new MainMenu(_game) );
		}
		return false;
	}
	
	public void playerMove(Cell nextCell) {
		_playerCell.decreaseResistance();
	    _playerCell = nextCell;
	}
	
	public Cell getCellByCoord (int x, int y) {
		Vector3 paramPos = new Vector3();
		Vector3 cellPos = new Vector3();
		
		paramPos.set(x, y, 0);
		_game.getCamera().unproject(paramPos);
		
		for (Cell result: _gameGrid.getCells()) {
			cellPos.set(result.getX(), result.getY(), 0);
			_game.getCamera().unproject(cellPos);
			
			if (paramPos.x >= cellPos.x &&  paramPos.x <= cellPos.x + Cell.CELL_SIZE) {
				if (paramPos.y >= cellPos.y && paramPos.y <= cellPos.y+Cell.CELL_SIZE) {
					return (result);
				}
			}
		}
		
		return(null);
	}
	
	public boolean playerCanGo(Cell nextCell) {
		int px = _playerCell.getX();
		int py = _playerCell.getY();
		int nx = nextCell.getX();
		int ny = nextCell.getY();
		
		// tratamento para nao mover para a mesma celula
		if (px == nx && py == ny) return (false);
		
		if (ny == py) { // proxima celula esta na mesma linha
			if (nx > px) { // player esta se movendo para cima
				if (nx - px <= Cell.CELL_SIZE*3) return (true);
			}
			
			else if (nx < px) { // player esta se movendo para baixo
				if (px - nx <= Cell.CELL_SIZE*3) return (true);
			}
		}
		
		else if (nx == px) { // proxima celula esta na mesma coluna
			if (ny > py) {
				if (ny-py <= Cell.CELL_SIZE*3) return (true);
			}
			
			else if (ny < py) {
				if (py-ny <= Cell.CELL_SIZE*3) return (true);
			}
		}
		
		return (false);
	}
	
	public void restart() {
		for (Cell cell: _gameGrid.getCells()) {
			cell.restart();
		}
		
		_tries = 0;
		_totalTime = 0;
		_playerCell = _originalPlayerCell;
	}
	
	
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		touchDown(screenX, screenY, pointer, 0);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	// configura o player
	private void configurePlayer() {
		if (_level != -1) {
			InputStream in = _game.getCharacterConfigurationFile(_level).read();
			InputStreamReader ireader = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(ireader);
			String currentLine = "";
			int row = -1;
			int col = -1;
			
			try {
				while ( (currentLine = br.readLine()) != null ) {
				
					if (currentLine.contains("row")) {
						row = Integer.parseInt(currentLine.split("=")[1]);
					}

					else if (currentLine.contains("col")) {
						col = Integer.parseInt(currentLine.split("=")[1]);
					}
				}
				
			} catch (IOException e) {
				
			}
			
			_playerCell = _gameGrid.getCell(row, col);
			_originalPlayerCell = _playerCell;
		}
	}
	
	
	// configuracao do level
	private void configureLevel() {
		if (_level != -1) {
			Preferences p = Gdx.app.getPreferences("preferences");
			p.putInteger("lastPlayedLevel", _level);
			p.flush();
			
			InputStream in = _game.getStageConfigurationFile(_level).read();
			InputStreamReader ireader = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(ireader);
			String currentLine;
			
			// representacao dos dados do cenario em linhas de string
			Array<String> scenario = new Array<String>();
			
			try {
				while ( (currentLine = br.readLine()) != null ) {
					// tratamentos irritantes
					currentLine = currentLine.replace(" ", "");
					
					if (currentLine.length() != 0) scenario.add(currentLine);
					else {
						//linhas vazias serao ignoradas
					}
				}
			} catch (IOException e) {
				Gdx.app.log("ERROR", "Erro lendo arquivo de cenario: data/Stages/"+_level);
			}
			
			if (scenario.size > 0) {
				int colsMax = scenario.get(0).length();
				int row = 1;
				int col = 1;
				int xini = Cell.getInitialX();
				int yini = Cell.getInitialY();
				int x = xini;
				int y = yini;
				int resistance = 0;
				
				// Instancio a game grid
				_gameGrid = new GameGrid(_game);
				
				// itero por cada uma das linhas lidas
				for (String line: scenario) {
					// interpreto a linha
					col = 1;
					x = xini;
					for (int i=0;i<line.length();i++) {
						HashMap<Integer, Integer> row_col = new HashMap<Integer, Integer>();
						row_col.put(row, col);
						resistance = Integer.parseInt(String.format("%c", line.charAt(i)));
						
						_gameGrid.addCell(row, col, x, y, resistance);
						
						col++;
						x += Cell.CELL_SIZE + Cell.CELL_GAP;
					}
					row++;
					y += Cell.CELL_SIZE + Cell.CELL_GAP;
				}
			}
		}
	}
}
