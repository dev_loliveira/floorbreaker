package com.redcode.floorbreaker;

import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;


public class MainMenu extends AbstractScreen {
	private enum AvailableLanguages {
		English("en_UK",    "English"),
		Portuguese("pt_BR", "Português"),
		Spanish("es_ES",    "Español"),
		German("de_DE",     "Alemán");
		
		private String _name;
		private String _code;
		
		private AvailableLanguages (String code, String name) {
			_code = code;
			_name = name;
		}
		
		private String getCode () {
			return (_code);
		}
		
		private String getName () {
			return (_name);
		}
		
		private static String getNameByCode(String code) {
			String name = null;
			for (AvailableLanguages l: AvailableLanguages.values()) {
				if (code == l.getCode()) {
					name = l.getName();
				}
			}
			return (name);
		}

		private static String getCodeByName(String name) {
			String code = null;
			for (AvailableLanguages l: AvailableLanguages.values()) {
				if (name.equals(l.getName())) {
					code = l.getCode();
				}
			}
			return (code);
		}
	}
	private DataState                   _state;
	private FloorBreaker                _game;
	private LanguagesManager            _lang;
	private Image                       _imgBackground;
	
	public MainMenu (FloorBreaker game) {
		super (game);
		_game = game;
		_state = DataState.Loading;
		if (_game.getRequestHandler() != null && Gdx.app.getType() == ApplicationType.Android)
			_game.getRequestHandler().showAds(false);
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		SpriteBatch batch = null;
		
		if (_state == DataState.Loading) {
			batch = _game.getSpriteBatch();
			
			TextBounds loadingBounds = _game.getFontLoading().getBounds(_lang.getString("Loading"));
			_game.getFontLoading().draw(
					batch, 
					_lang.getString("Loading..."), 
					FloorBreaker.VIRTUAL_WIDTH/2 - loadingBounds.width/2, 
					FloorBreaker.VIRTUAL_HEIGHT/2 - loadingBounds.height/2);
		}
		
		else {
			batch = getStage().getSpriteBatch();
			getStage().act(delta);
			getStage().draw();
		}
	}

	@Override
	public void load() {
		AssetManager assetManager = _game.getAssetManager();
		_lang = LanguagesManager.getInstance();
		assetManager.load("data/click.wav", Sound.class);
		assetManager.load("data/mainmenuBackground.png", Texture.class);
		
		// FIXME: assetManager.load("data/background.png", Texture.class);
	}

	@Override
	public void unload() {
		AssetManager assetManager = _game.getAssetManager();
		
		assetManager.unload("data/click.wav");
		assetManager.unload("data/mainmenuBackground.png");
		//assetManager.unload("data/background.png");
	}

	@Override
	public void assignResources() {
		AssetManager assetManager = _game.getAssetManager();
		_imgBackground = new Image(assetManager.get("data/mainmenuBackground.png", Texture.class));
		
		// pequeno ajuste na font do menu
		TextButtonStyle buttonStyle = new TextButtonStyle();
		buttonStyle = getSkin().get("default", TextButtonStyle.class);
		buttonStyle.font = _game.getFontLoading();

		// pequeno ajuste na fonte dos selects tambem
		SelectBoxStyle selectboxStyle = new SelectBoxStyle(getSkin().get("default", SelectBoxStyle.class));
		selectboxStyle.font = _game.getFontLoading();
		
		
		Table table = new Table();
		table.setFillParent(true);
		table.setY(0);
		
		Image logo = new Image(_game.getLogo());
		logo.setX((FloorBreaker.VIRTUAL_WIDTH - _game.getLogo().getWidth())/2);
		logo.setY(FloorBreaker.VIRTUAL_HEIGHT-_game.getLogo().getHeight());
		
		getStage().addActor(_imgBackground);
		getStage().addActor(logo);
		getStage().addActor(table);
		
		/*
		 * O nome dos elementos sera a chave utilizada no mapa
		 * de idiomas.
		 * 
		 * */

		//final TextButton startGameButton = new TextButton(_lang.getString("Start Game"), getSkin().get("default", TextButtonStyle.class));
		final TextButton startGameButton = new TextButton(_lang.getString("Start Game"), buttonStyle);
		startGameButton.setName("Start Game");
		startGameButton.setChecked(false);
		startGameButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (_game.isSoundEnabled()) _game.getClickSound().play();
				_game.setCurrentScreen(new MenuStartGame(_game));
			}
		});
		
		//final TextButton soundButton = new TextButton(_lang.getString("Disable sound"), getSkin().get("toggle", TextButtonStyle.class));
		final TextButton soundButton = new TextButton(_lang.getString("Disable sound"), buttonStyle);
		soundButton.setChecked(_game.isSoundEnabled());
		soundButton.setName(_game.isSoundEnabled() ? "Disable sound" : "Enable sound");
		soundButton.setText(_game.isSoundEnabled() ? _lang.getString("Disable sound") : _lang.getString("Enable sound"));
		soundButton.getLabel().setFontScale(1);
		soundButton.addListener(new ClickListener(){
			public void clicked (InputEvent event, float x, float y) {
				if (_game.isSoundEnabled()) {
					_game.disableSound();
					soundButton.setText(_lang.getString("Enable sound"));
				}
				else {
					_game.enableSound();
					soundButton.setText(_lang.getString("Disable sound"));
				}
				
				if (_game.isSoundEnabled()) _game.getClickSound().play();
				soundButton.setName(_game.isSoundEnabled() ? "Disable sound" : "Enable sound");
			}
		});
		
		final TextButton howtoplayButton = new TextButton(_lang.getString("How to play"), buttonStyle);
		howtoplayButton.setChecked(false);
		howtoplayButton.setName("How to play");
		howtoplayButton.setText(_lang.getString("How to play"));
		howtoplayButton.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				if (_game.isSoundEnabled()) _game.getClickSound().play();
				_game.setCurrentScreen( new HowToPlayScreen(_game) );
			}
		});
		
		//final TextButton exitButton = new TextButton(_lang.getString("Exit"), getSkin().get("default", TextButtonStyle.class));
		final TextButton exitButton = new TextButton(_lang.getString("Exit"), buttonStyle);
		exitButton.setChecked(false);
		exitButton.setName("Exit");
		exitButton.setText(_lang.getString("Exit"));
		exitButton.addListener(new ClickListener() {
			public void clicked (InputEvent event, float x, float y) {
				Gdx.app.exit();
			}
		});
		
		// Select para idiomas
		Array<String> langArray = new Array<String>();
		for (AvailableLanguages lang: AvailableLanguages.values()) {
			langArray.add( lang.getName() );
		}
		Object[] langObjects = new Object[langArray.size];
		for (int i=0;i < langArray.size;i++) {
			langObjects[i] = langArray.get(i);
		}
		
		final SelectBox langSelect = new SelectBox(langObjects, selectboxStyle);
		langSelect.setWidth(400f);
		langSelect.addListener( new ChangeListener () {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				String langCode = AvailableLanguages.getCodeByName(langSelect.getSelection());
				_lang.loadLanguage(langCode);
				
				startGameButton.getLabel().setText(
						_lang.getString(startGameButton.getName()
								));
				
				soundButton.getLabel().setText(
						_lang.getString(soundButton.getName()));
				
				exitButton.getLabel().setText(
						_lang.getString(exitButton.getName()));
				
				howtoplayButton.getLabel().setText(
						_lang.getString(howtoplayButton.getName())
						);
			}
			
		});
		langSelect.setSelection(AvailableLanguages.getNameByCode(_lang.getLanguage()));
		
		table.setName("table");
		table.add(startGameButton).size(BUTTON_WIDTH, BUTTON_HEIGHT).uniform().spaceBottom(30); table.row();
		//table.add(howtoplayButton).uniform().spaceBottom(30).fill(); table.row();
		table.add(soundButton).uniform().spaceBottom(30).fill(); table.row();
		table.add(langSelect).uniform().spaceBottom(30).fill(); table.row();
		table.add(exitButton).uniform().spaceBottom(30).fill(); table.row();
		table.setWidth(400f);
		table.debug();
	}
	
	@Override
	public void update (double delta) {
		AssetManager assetManager = _game.getAssetManager();
		if (_state == DataState.Loading) {
			if (assetManager.update()) {
					assignResources();
					_state = DataState.Loaded;
			}
			
			else {
				_state = DataState.Loading;
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
