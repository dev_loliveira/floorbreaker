package com.redcode.floorbreaker;

public interface DataManagement {
	    public enum DataState {
	    	Loading,
	    	Loaded
	    }
        public void load();

        public void unload();

        public void assignResources();

}