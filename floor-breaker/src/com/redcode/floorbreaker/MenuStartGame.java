package com.redcode.floorbreaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.redcode.floorbreaker.DataManagement.DataState;

public class MenuStartGame extends AbstractScreen {
	private final int                  STAGE_PREVIEW_SIZE = 210;
	private DataState                   _state;
	private FloorBreaker                _game;
	private LanguagesManager            _lang;
	private int                         _levelIndex;
	private Image                       _previewStage;
	
	public MenuStartGame (FloorBreaker game) {
		super(game);
		_game = game;
		_state = DataState.Loading;
	}

	@Override
	public void render(float delta) {
		SpriteBatch batch = null;
		
		if (_state == DataState.Loading) {
			batch = _game.getSpriteBatch();
			
			TextBounds loadingBounds = _game.getFontLoading().getBounds(_lang.getString("Loading"));
			_game.getFontLoading().draw(
					batch, 
					_lang.getString("Loading..."), 
					FloorBreaker.VIRTUAL_WIDTH/2 - loadingBounds.width/2, 
					FloorBreaker.VIRTUAL_HEIGHT/2 - loadingBounds.height/2);
		}
		
		else {
			SpriteBatch stageBatch = getStage().getSpriteBatch();
			
			getStage().act(delta);
			getStage().draw();
			
			if (_previewStage != null) {
				stageBatch.begin();
				_previewStage.draw(stageBatch, 1);
				stageBatch.end();
			}
		}
	}


	@Override
	public void load() {
		AssetManager assetManager = _game.getAssetManager();
		_lang = LanguagesManager.getInstance();
		assetManager.load("data/click.wav", Sound.class);
		
		// FIXME: assetManager.load("data/background.png", Texture.class);
	}

	@Override
	public void unload() {
		AssetManager assetManager = _game.getAssetManager();
		
		assetManager.unload("data/click.wav");
		//assetManager.unload("data/background.png");
	}

	@Override
	public void assignResources() {
		AssetManager assetManager = _game.getAssetManager();
		
		final Preferences p = Gdx.app.getPreferences("preferences");
		
		if (p.getInteger("lastUnlockedLevel") == 0) {
			p.putInteger("lastUnlockedLevel", 1);
			p.flush();
		}
		
		LabelStyle labelStyle = new LabelStyle(getSkin().get("default", LabelStyle.class));
		labelStyle.font = _game.getFontLoading();

		TextButtonStyle buttonStyle = new TextButtonStyle();
		buttonStyle = getSkin().get("default", TextButtonStyle.class);
		buttonStyle.font = _game.getFontLoading();

		SelectBoxStyle selectboxStyle = new SelectBoxStyle(getSkin().get("default", SelectBoxStyle.class));
		selectboxStyle.font = _game.getFontLoading();
		
		final Label labelSelectScenario = new Label(_lang.getString("Stage selection"), labelStyle);
		final Label labelScenario = new Label(String.format("%s: #%d", _lang.getString("Stage"), 1), labelStyle);
		
		/*
		final Object[] items = new Object[p.getInteger("lastLevel")+30];
		for (int i=0;i<p.getInteger("lastLevel")+30;i++) {
			items[i] = new String(String.format("Level %d", i+1));
		}
		final SelectBox selectScenario = new SelectBox (items, selectboxStyle);
		
		selectScenario.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				System.out.println("changed to: " + items[selectScenario.getSelectionIndex()]);
			}
			
		});
		*/

		int sliderMax = p.getInteger("lastUnlockedLevel");
		while (_game.stageExists(sliderMax) == false && sliderMax > 1) sliderMax--;
		if (sliderMax <= 0) {
			Gdx.app.log("ERROR", String.format("Slider index not valid! [%d]", sliderMax));
			Gdx.app.exit();
		}
		
		final Slider slider = new Slider(1, sliderMax, 1, false, getSkin());
		if (p.getInteger("lastUnlockedLevel") > 1) {
			slider.setValue((float)p.getInteger("lastPlayedLevel"));

			_previewStage = getPreviewStage(p.getInteger("lastPlayedLevel"));
			_previewStage.setName("previewStage");
			
			labelScenario.setText( String.format("%s: #%d", _lang.getString("Stage"), (int)slider.getValue()) );
			slider.addListener(new ChangeListener(){
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					labelScenario.setText(String.format("%s: #%d", _lang.getString("Stage"), (int)slider.getValue()));
					_previewStage = getPreviewStage((int)slider.getValue());
				}});
		}
		
		
		final TextButton startGameButton = new TextButton(_lang.getString("Start"), buttonStyle);
		startGameButton.addListener(new ClickListener(){
			public void clicked (InputEvent event, float x, float y) {
				if (_game.isSoundEnabled()) _game.getClickSound().play();
				_game.setCurrentScreen( new GameCore(_game, (int)slider.getValue()) );
			}
		});

		final TextButton backButton = new TextButton(_lang.getString("Back"), buttonStyle);
		backButton.addListener(new ClickListener(){
			public void clicked (InputEvent event, float x, float y) {
				if (_game.isSoundEnabled()) _game.getClickSound().play();
				_game.setCurrentScreen (new MainMenu(_game));
			}
		});
		
		Table table = new Table();
		table.setName("table");
		table.setFillParent(true);
		super.getStage().addActor(_game.getBackground());
		super.getStage().addActor(table);
		

		table.add(labelScenario); table.row();
		if (p.getInteger("lastUnlockedLevel") > 1) {
			//table.add(_previewStage).uniform().size(_previewStage.getWidth(), _previewStage.getHeight()).padBottom(30); table.row();
			table.add(slider).size(BUTTON_WIDTH*2, 100); table.row();
		}
		table.add(startGameButton).size(BUTTON_WIDTH, BUTTON_HEIGHT).padBottom(30); table.row();
		table.add(backButton).size(BUTTON_WIDTH, BUTTON_HEIGHT);
	}
	
	public void update (double delta) {
		AssetManager assetManager = _game.getAssetManager();
		if (_state == DataState.Loading) {
			if (assetManager.update()) {
					assignResources();
					_state = DataState.Loaded;
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	// funcao para calcular o preview do cenario
	public Image getPreviewStage(int stage) {
		Pixmap presistance1 = new Pixmap(Gdx.files.internal("data/previewcellresistance1.png"));
		Pixmap presistance2 = new Pixmap(Gdx.files.internal("data/previewcellresistance2.png"));
		Pixmap pemptycell = new Pixmap(Gdx.files.internal("data/previewcellempty.png"));
		Pixmap pplayer = new Pixmap(Gdx.files.internal("data/previewplayer.png"));
		Texture textureGlobal = new Texture(pemptycell.getWidth()*14, pemptycell.getHeight()*7, Format.RGBA8888);
		
		/*
		 * 1) ler arquivo de configuracao do stage
		 * 2) iterar sobre sua configuracao
		 * 3) adicionar a devida imagem em textureGlobal na coordenada correta
		 */

		InputStream playerin = _game.getCharacterConfigurationFile(stage).read();
		InputStreamReader playerireader = new InputStreamReader(playerin);
		BufferedReader playerbr = new BufferedReader(playerireader);
		String playercurrentLine = "";
		int playerrow = -1;
		int playercol = -1;
		
		try {
			while ( (playercurrentLine = playerbr.readLine()) != null ) {
			
				if (playercurrentLine.contains("row")) {
					playerrow = Integer.parseInt(playercurrentLine.split("=")[1]);
				}

				else if (playercurrentLine.contains("col")) {
					playercol = Integer.parseInt(playercurrentLine.split("=")[1]);
				}
			}
			
		} catch (IOException e) {
			
		}
		
		
		
		
		
		InputStream in = _game.getStageConfigurationFile(stage).read();
		InputStreamReader ireader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(ireader);
		String currentLine;
		
		// representacao dos dados do cenario em linhas de string
		Array<String> scenario = new Array<String>();
		
		try {
			while ( (currentLine = br.readLine()) != null ) {
				// tratamentos irritantes
				currentLine = currentLine.replace(" ", "");
				
				if (currentLine.length() != 0) scenario.add(currentLine);
				else {
					//linhas vazias serao ignoradas
				}
			}
		} catch (IOException e) {
			Gdx.app.log("ERROR", "Erro lendo arquivo de cenario: data/Stages/"+stage);
		}
		
		if (scenario.size > 0) {
			int xini = 0;
			int yini = 0;
			int x = xini;
			int y = yini;
			int row = 1;
			int col = 1;
			
			for (String line: scenario) {
				x = xini;
				col = 1;
				for (int i=0;i<line.length();i++) {
					int cellValue = Integer.parseInt(String.format("%s", line.charAt(i)));
					switch (cellValue) {
						case 0:
							textureGlobal.draw(pemptycell, x, y);
							break;

						case 1:
							textureGlobal.draw(presistance1, x, y);
							break;

						case 2:
							textureGlobal.draw(presistance2, x, y);
							break;
					}
					
					if (row == playerrow && col == playercol) {
						textureGlobal.draw(pplayer, x+pplayer.getWidth()/2, y+pplayer.getHeight()/2);
					}
					
					x += pemptycell.getWidth();
					col++;
				}
				y += pemptycell.getHeight();
				row++;
			}
		}
		
		Image preview = new Image(textureGlobal);
		preview.setPosition(
				(FloorBreaker.VIRTUAL_WIDTH-STAGE_PREVIEW_SIZE)/2,
				FloorBreaker.VIRTUAL_HEIGHT-STAGE_PREVIEW_SIZE
				);
		preview.setSize(STAGE_PREVIEW_SIZE, STAGE_PREVIEW_SIZE);
		
		presistance1.dispose();
		presistance2.dispose();
		pemptycell.dispose();
		
		return (preview);
	}

}
