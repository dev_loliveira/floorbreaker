package com.redcode.floorbreaker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.redcode.floorbreaker.DataManagement.DataState;

public class SplashScreen extends AbstractScreen implements InputProcessor {
	
	private final int             MAX_TIME = 3;
	
	private FloorBreaker           _game;
	private Texture                _imgBackground;
	private DataState              _state;
	private float                  _elapsedTime;
	
	public SplashScreen(FloorBreaker game) {
		super(game);
		_game = game;
		_state = DataState.Loading;
		Gdx.input.setInputProcessor(this);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
	}

	@Override
	public void load() {
		AssetManager assetManager = _game.getAssetManager();
		
		assetManager.load("data/splashScreen.png", Texture.class);
	}
	
	@Override
	public void unload() {
		AssetManager assetManager = _game.getAssetManager();
		
		assetManager.unload("data/splashScreen.png");
	}

	@Override
	public void assignResources() {
		AssetManager assetManager = _game.getAssetManager();
		
		_imgBackground = assetManager.get("data/splashScreen.png");
		
		_elapsedTime = 0;
	}
	
	@Override
	public void update(double delta) {
		AssetManager assetManager = _game.getAssetManager();
		if (_state == DataState.Loading) {
			if (assetManager.update()) {
					assignResources();
					_state = DataState.Loaded;
			}
			
			else {
				_state = DataState.Loading;
			}
		}
	}
	
	@Override
	public void render(float delta) {
		if (_state == DataState.Loaded) {
			SpriteBatch batch = _game.getSpriteBatch();
			batch.draw(_imgBackground, 0, 0);
			
			batch.draw(
					_game.getLogo(),
					(FloorBreaker.VIRTUAL_WIDTH - _game.getLogo().getWidth())/2,
					(FloorBreaker.VIRTUAL_HEIGHT-_game.getLogo().getHeight())
					);
			_elapsedTime += delta;
			if (_elapsedTime > MAX_TIME) {
				_game.setCurrentScreen( new MainMenu(_game) );
			}
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		_game.setCurrentScreen( new MainMenu(_game) );
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
