package com.redcode.floorbreaker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Array;

public class HowToPlayScreen extends AbstractScreen implements InputProcessor {
	private enum State {
		Loading,
		Running;
	}
	
	private enum TutorialMessages {
		MessageA("tutorial_01"),
		MessageB("tutorial_02"),
		MessageC("tutorial_03");
		
		private String message;
		
		private TutorialMessages(String message) {
			this.message = message;
		}
		
		private String getMessage() {
			return (this.message);
		}
	}
	
	private enum TutorialTexture {
		Texture1("data/tutorial/playing_01.png"),
		Texture2("data/tutorial/playing_02.png"),
		Texture3("data/tutorial/menuSelectScenario_slider.png");
		
		private String fpath;
		
		private TutorialTexture (String fpath) {
			this.fpath = fpath;
		}
		
		private String getFilePath() {
			return (this.fpath);
		}
	}
	
	private FloorBreaker                       _game;
	private State                           _state;
	private int                             _tutorialIndex;
	private Array<String>                   _tutorialMessages;
	private Array<TextureRegion>            _tutorialTextures;
	private LanguagesManager                _lang;
	private ShapeRenderer                   _shape;
	private boolean                        _showPageChange;
	private float                          _pageChangeInc;
	private float                          _pageChangeLast;
	private float                          _pageChangeFreq;
	
	public HowToPlayScreen (FloorBreaker game) {
		super(game);
		_game = game;
		_state = State.Loading;
		_lang = LanguagesManager.getInstance();
	}
	
	public String getCorrectString(String str) {
		String result = "";
		
		for (int i=0;i<str.length();i++) {
			if (str.charAt(i) == '\\' && i+1 < str.length() && str.charAt(i+1) == 'n') {
				result += '\n';
				i++;
			}
			
			else {
				result += str.charAt(i);
			}
		}
		
		return (result);
	}
	
	@Override
	public void render (float delta) {
		SpriteBatch batch = _game.getSpriteBatch();
		
		if (_state == State.Loading) {
			String loadingStr = _lang.getString("Loading...") + Integer.toString((int)_game.getAssetManager().getProgress());
			TextBounds loadingBounds = _game.getFontLoading().getBounds(loadingStr);
			_game.getFontLoading().draw(
					batch, loadingStr, 
					(FloorBreaker.VIRTUAL_WIDTH-loadingBounds.width)/2,
					(FloorBreaker.VIRTUAL_HEIGHT-loadingBounds.height)/2
					);
		}
		
		else if (_state == State.Running) {
			if (_tutorialIndex < _tutorialMessages.size && _tutorialIndex < _tutorialTextures.size) {
				String message = _lang.getString( _tutorialMessages.get(_tutorialIndex) );
				TextBounds messageBounds = _game.getFontLoading().getBounds(message);
				_game.getBackground().draw(batch, 1);

				// Adding the images
				batch.draw(
						_tutorialTextures.get(_tutorialIndex),
						(FloorBreaker.VIRTUAL_WIDTH-_tutorialTextures.get(_tutorialIndex).getRegionWidth())/2,
						(FloorBreaker.VIRTUAL_HEIGHT-_tutorialTextures.get(_tutorialIndex).getRegionHeight())
						);

				// Adding the messages
				_game.getFontLoading().drawMultiLine(
						batch, getCorrectString(message), 
						//(Lightsout.VIRTUAL_WIDTH-messageBounds.width)/2,
						100,
						(FloorBreaker.VIRTUAL_HEIGHT)/2 - (50));
				
				// Adding page count
				TextBounds pageCountBounds = _game.getFontLoading().getBounds(String.format("%d/%d", _tutorialIndex, TutorialMessages.values().length));
				_game.getFontLoading().draw(
						batch,
						String.format("%d/%d", _tutorialIndex+1, TutorialMessages.values().length),
						FloorBreaker.VIRTUAL_WIDTH-pageCountBounds.width/2-60, 
						pageCountBounds.height/2+60
						);
				
				if(_pageChangeInc-_pageChangeLast >= _pageChangeFreq) {
					_pageChangeLast = _pageChangeInc;
					_showPageChange = !_showPageChange;
				}
				_pageChangeInc += delta;
				
				if (_showPageChange) {
					_shape.begin(ShapeType.Circle);
					_shape.setColor(0f, 0f, 0f, 1);
					_shape.circle(
							FloorBreaker.VIRTUAL_WIDTH-60, 
							60, 
							40
							);
					_shape.end();
				}
			}
			
			else {
				_game.setCurrentScreen( new MainMenu(_game) );
			}
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		_tutorialMessages = null;
	}

	@Override
	public void load() {
		AssetManager assetManager = _game.getAssetManager();
		
		_shape = new ShapeRenderer();
		
		_tutorialIndex = 0;
		_tutorialMessages = new Array<String>();
		_tutorialTextures = new Array<TextureRegion>();
		
		_showPageChange = false;
		_pageChangeFreq = 1.5f;
		_pageChangeLast = 0;
		_pageChangeInc = 0;

		for (TutorialTexture tutorialTexture: TutorialTexture.values()) {
			assetManager.load(tutorialTexture.getFilePath(), Texture.class);
		}
	}

	@Override
	public void unload() {
		AssetManager assetManager = _game.getAssetManager();
		
		_tutorialIndex = 0;
		_tutorialMessages = null;
		_tutorialTextures = null;

		for (TutorialTexture tutorialTexture: TutorialTexture.values()) {
			assetManager.unload(tutorialTexture.getFilePath());
		}
	}

	@Override
	public void assignResources() {
		AssetManager assetManager = _game.getAssetManager();
		
		for (TutorialMessages tutorial: TutorialMessages.values()) {
			_tutorialMessages.add(tutorial.getMessage());
		}
		

		for (TutorialTexture tutorialTexture: TutorialTexture.values()) {
			_tutorialTextures.add( new TextureRegion(assetManager.get(tutorialTexture.getFilePath(), Texture.class)) );
		}
		
		Gdx.input.setInputProcessor(this);
	}
	
	@Override
	public void update (double delta) {
		AssetManager assetManager = _game.getAssetManager();
		if (_state == State.Loading) {
			if (assetManager.update()) { // finished loading resources
				assignResources();
				_state = State.Running;
			}
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		_tutorialIndex++;
		_showPageChange = false;
		_pageChangeInc = 0;
		_pageChangeLast = 0;
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
