package com.redcode.floorbreaker;

/*
 *
 * Credits:
 *   click.wav: by Mike Koenig from soundbible.com
 *   floor_destruction.wav: from soundjay.com
 *   
 *   
 */


import java.util.HashMap;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class FloorBreaker implements ApplicationListener {

	public static final int            VIRTUAL_WIDTH = 1280;
	public static final int            VIRTUAL_HEIGHT = 720;
	private static final float         ASPECT_RATIO = 1.7777f;
	
	private Rectangle                    _viewport;
	private AssetManager                 _assetsManager;
	private LanguagesManager             _lang;
	private OrthographicCamera           _camera;
	private SpriteBatch                  _batch;
	private static PlatformResolver     _resolver;
	private GameCore                     _gameCore;
	private MainMenu                     _mainMenu;
	private boolean                     _soundEnabled;
	private AbstractScreen               _currentScreen;
	private AbstractScreen               _lastScreen;
	private AbstractScreen               _nextScreen;
	private Image                        _imgBackground;
	private BitmapFont                   _fontLoading;
	private Sound                        _sndClick;
	private Sound                        _sndFloorDestruction;
	private Texture                       _logo;
	private IActivityRequestHandler      _requestHandler;

	private FPSLogger                    fpslogger;
	
	public FloorBreaker(IActivityRequestHandler requestHandler) {
		_requestHandler = requestHandler;
		if (_requestHandler != null)
		    _requestHandler.showAds(false);
	}
	
	
	@Override
	public void create () {
		_assetsManager = new AssetManager();
		//_assetsManager.finishLoading();
		
		Texture.setEnforcePotImages(false);
		
		_lang = LanguagesManager.getInstance();
		_camera = new OrthographicCamera(VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
		_camera.setToOrtho(false, VIRTUAL_WIDTH, VIRTUAL_HEIGHT);
		_batch = new SpriteBatch();
		
		fpslogger = new FPSLogger();

		enableSound();
		load();
		
		//_currentScreen = new MainMenu(this);
		_currentScreen = new SplashScreen(this);
		_currentScreen.load();
		
		//Gdx.input.setCursorCatched(true);
		Gdx.input.setCatchBackKey(true);
	}
	
	public void load() {
		_imgBackground = new Image(new Texture(Gdx.files.internal("data/background.png")));
		_sndClick = Gdx.audio.newSound(Gdx.files.internal("data/click.wav"));
		_sndFloorDestruction = Gdx.audio.newSound(Gdx.files.internal("data/floor_destruction.wav"));
		
		_fontLoading = _resolver.loadFont("data/default.fnt", "data/mksanstallx.ttf", 30);
		_logo = new Texture(Gdx.files.internal("data/logo.png"));
	}
	
	public Texture getLogo() {
		return (_logo);
	}
	
	public Sound getClickSound() {
		return (_sndClick);
	}
	
	public Sound getFloorDestructionSound() {
		return (_sndFloorDestruction);
	}
	
	public Image getBackground () {
		return (_imgBackground);
	}
	
	public void setCurrentScreen (AbstractScreen screen) {
		_nextScreen = screen;
	}
	
	public AbstractScreen getCurrentScreen () {
		return (_currentScreen);
	}
	
	public void enableSound() {
		_soundEnabled = true;
	}
	
	public void disableSound () {
		_soundEnabled = false;
	}
	
	public boolean isSoundEnabled () {
		return (_soundEnabled);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f,0);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		_camera.update();

		Gdx.gl.glViewport((int)_viewport.x, (int)_viewport.y,
				          (int)_viewport.width, (int)_viewport.height
				);
		
		_batch.begin();
		_batch.setProjectionMatrix(_camera.combined);
		    _currentScreen.update(Gdx.graphics.getDeltaTime());
		    _currentScreen.render(Gdx.graphics.getDeltaTime());
		_batch.end();
		
		performPendingScreenChange();
		performPendingUnload();
		
		//fpslogger.log();
	}
	
	public void performPendingScreenChange() {
		if (_nextScreen != null) {
			_lastScreen = _currentScreen;
			_currentScreen = _nextScreen;
			_currentScreen.load();
			_nextScreen = null;
		}
	}
	
	public void performPendingUnload() {
		if (_currentScreen != null) {
			if (_lastScreen != null) {
				_lastScreen.unload();
				_lastScreen = null;
			}
		}
	}

	@Override
	public void resize (int width, int height) {
		// calculate new viewport
        float aspectRatio = (float)width/(float)height;
        float scale = 1f;
        Vector2 crop = new Vector2(0f, 0f);
        
        if(aspectRatio > ASPECT_RATIO)
        {
            scale = (float)height / (float)VIRTUAL_HEIGHT;
            crop.x = (width - VIRTUAL_WIDTH * scale) / 2.0f;
        }
        else if(aspectRatio < ASPECT_RATIO)
        {
            scale = (float)width / (float)VIRTUAL_WIDTH;
            crop.y = (height - VIRTUAL_HEIGHT * scale) / 2.0f;
        }
        else
        {
            scale = (float)width/(float)VIRTUAL_WIDTH;
        }

        float w = (float)VIRTUAL_WIDTH * scale;
        float h = (float)VIRTUAL_HEIGHT * scale;
        _viewport = new Rectangle(crop.x, crop.y, w, h);
	}

	@Override
	public void dispose () {
		_assetsManager.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}
	
	public AssetManager getAssetManager() {
		return (_assetsManager);
	}
	
	public OrthographicCamera getCamera() {
		return (_camera);
	}
	
	public SpriteBatch getSpriteBatch() {
		return (_batch);
	}
	
	public static PlatformResolver getPlatformResolver() {
		return (_resolver);
	}
	
	public static void setPlatformResolver(PlatformResolver resolver) {
		_resolver = resolver;
	}
	
	public Rectangle getViewport() {
		return (_viewport);
	}
	
	public BitmapFont getFontLoading () {
		return (_fontLoading);
	}
	
	public boolean stageExists(int stage) {
		return (Gdx.files.internal( String.format("data/Stages/%d", stage) ).exists());
	}
	
	public FileHandle getStageConfigurationFile(int stage) {
		return (Gdx.files.internal( String.format("data/Stages/%d", stage) ));
	}
	
	public FileHandle getCharacterConfigurationFile(int stage) {
		return (Gdx.files.internal( String.format("data/Stages/%d_player", stage) ));
	}
	
	public IActivityRequestHandler getRequestHandler () {
		return (_requestHandler);
	}
}
