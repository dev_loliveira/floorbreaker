package com.redcode.floorbreaker;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main(String[] args) {
		FloorBreaker.setPlatformResolver( new DesktopResolver() );
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Floorbreaker";
		cfg.useGL20 = false;
		cfg.width = 1280;
		cfg.height = 720;
		
		new LwjglApplication(new FloorBreaker(null), cfg);
	}
}
