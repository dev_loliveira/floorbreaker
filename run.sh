###########################################################
# Script está reutilizável para outros projetos do libgdx #
###########################################################


get_project_name() {
    items=`ls | grep -`
    platform=$1
    for item in ${items[@]}
    do
        # Testa se o arquivo / diretorio possui em seu nome a string referente a plataforma
        # sendo configurada. Caso possua realiza um echo de seu nome excluindo o nome
        # da plataforma
        test "${item#*$platform}" != "$item" && echo "$item"  | sed "s/-$platform//g"
    done
}


stringfy() {
    arr=$1
    concat=$2
    buff=""

    for item in ${arr[@]}
    do
        buff="$buff$item$concat"
    done

    echo $buff
}


# Para descobrir o nome do projeto
project=$(get_project_name "desktop")
platform="desktop"
libs=`find $project -name "*.jar"`
platform_libs=`find $project-$platform -name "*.jar"`
sources=`find $project -name "*.java"`
platform_sources=`find $project-$platform -name "*.java"`
sources_str=""
bin_dir="bin/"
main_class="com.redcode.floorbreaker.DesktopLauncher"


libs_str=$(stringfy "$libs" ":")
libs_str="$libs_str"$(stringfy "$platform_libs" ":")
libs_str="$libs_str:$bin_dir/$platform"
sources_str=$(stringfy "$sources" " ")
platform_sources_str=$(stringfy "$platform_sources" " ")

java -cp $libs_str $main_class
