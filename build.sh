###########################################################
# Script está reutilizável para outros projetos do libgdx #
###########################################################



get_project_name() {
    items=`ls | grep -`
    platform=$1
    for item in ${items[@]}
    do
        # Testa se o arquivo / diretorio possui em seu nome a string referente a plataforma
        # sendo configurada. Caso possua realiza um echo de seu nome excluindo o nome
        # da plataforma
        test "${item#*$platform}" != "$item" && echo "$item"  | sed "s/-$platform//g"
    done
}


stringfy() {
    arr=$1
    concat=$2
    buff=""

    for item in ${arr[@]}
    do
        buff="$buff$item$concat"
    done

    echo $buff
}


if [ -z "$1" ]; then platform="desktop"; else platform="$1" ;fi
if [ -z "$2" ]; then resource_dir="bin/data"; else resource_dir="$2" ;fi


# Para descobrir o nome do projeto
project=$(get_project_name "$platform")


if [ ! -d "$project-$platform" ]; then
    echo Alvo não $platform existe!
else
    if [ ! -d "$project-$platform/$resource_dir" ]; then
        echo Diretório de resources [$project-$platform/$resource_dir] não foi encontrado!
    fi
fi

libs=`find $project -name "*.jar"`
platform_libs=`find $project-$platform -name "*.jar"`
sources=`find $project -name "*.java"`
platform_sources=`find $project-$platform -name "*.java"`
bin_dir="bin"

if [ ! -d "$bin_dir" ]; then
    mkdir -p $bin_dir/$platform
else
    rm $bin_dir/$platform/* -rf
fi


libs_str=$(stringfy "$libs" ":")
libs_str="$libs_str"$(stringfy "$platform_libs" ":")
sources_str=$(stringfy "$sources" " ")
platform_sources_str=$(stringfy "$platform_sources" " ")


# Build e copia os resources para o diretorio do classpath
javac -d $bin_dir/$platform -cp $libs_str $sources_str $platform_sources
cp -r $project-$platform/$resource_dir $bin_dir/$platform
